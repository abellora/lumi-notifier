from apscheduler.schedulers.background import BlockingScheduler
from datetime import datetime
from email.mime.text import MIMEText
from email.header import Header
from math import isclose
import pytz
import smtplib
import os

from utils.oms_handler import *
from utils.dbod_handler import dbod_handler

# Configuration
DEBUG = False
CHECK_INTERVAL = 1 # minutes
DEL_LUMI_THRESHOLD = 5 # fb-1
REC_LUMI_THRESHOLD = 99999 # fb-1
DEL_LUMI_THRESHOLD_REPEAT = 1 # fb-1
REC_LUMI_THRESHOLD_REPEAT = 1 # fb-1

SMTP_SERVER = 'cernmx.cern.ch'
SMTP_PORT = 25

if os.getenv('DEV') is not None or DEBUG:
    LUMIDB_NAME = 'lumidb_2023_test'
    MOVEMENTDB_NAME = 'RPixMotorsDB_2023'
    NOTIFY_EMAIL = 'pps-lumi-notifier-dev@cern.ch'
else:
    LUMIDB_NAME = 'lumidb_2023'
    MOVEMENTDB_NAME = 'RPixMotorsDB_2023'
    NOTIFY_EMAIL = 'pps-lumi-notifier@cern.ch'

def notify(dbod:dbod_handler):
    """Send an email notification to the recipients in the DB
    """

    notification_time, move_del_lumi, move_rec_lumi, del_lumi_diff, rec_lumi_diff = dbod.getLastNotification()
    tz = pytz.timezone('Europe/Paris')
    time = notification_time.replace(tzinfo=pytz.utc).astimezone(tz)

    # Compute current delivered and recorded luminosity at notification time
    tot_del_lumi = move_del_lumi + del_lumi_diff
    tot_rec_lumi = move_rec_lumi + rec_lumi_diff

    # Email details
    sender = 'pps-lumi-notifier-noreply@cern.ch'
    subject = 'Lumi threshold reached'

    message = time.strftime('%Y-%m-%d %H:%M:%S (%Z)')+' - LUMI THRESHOLD REACHED!\n'
    if DEL_LUMI_THRESHOLD < 20:
        message += 'Delivered lumi threshold: '+str(DEL_LUMI_THRESHOLD)+' 1/fb\n'
        message += 'Delivered lumi since last movement: '+str(del_lumi_diff/1000)+' 1/fb\n'
        message += 'Current delivered lumi: '+str(tot_del_lumi/1000)+' 1/fb\n'
    if REC_LUMI_THRESHOLD < 20:
        message += 'Recorded lumi threshold: '+str(REC_LUMI_THRESHOLD)+' 1/fb\n'
        message += 'Recorded lumi since last movement: '+str(del_lumi_diff/1000)+' 1/fb\n'
        message += 'Current recorded lumi: '+str(tot_rec_lumi/1000)+' 1/fb\n'
    

    # Create the message
    msg = MIMEText(message, 'plain', 'utf-8')
    msg['From'] = Header(sender, 'utf-8')
    msg['To'] = NOTIFY_EMAIL
    msg['Subject'] = Header(subject, 'utf-8')

    try:
        # Connect to the SMTP server
        server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        server.starttls()

        # Send the email
        server.sendmail(sender, NOTIFY_EMAIL, msg.as_string())

        if DEBUG:
            print('Email sent successfully to '+NOTIFY_EMAIL)

    except smtplib.SMTPException as e:
        print('Error sending the email alarm:', str(e))

    finally:
        # Close the connection
        server.quit()


def monitor(dbod:dbod_handler):

    now = datetime.utcnow()
    # Monitor the DB and send an email if the thresholds are reached
    print(str(now)+': monitor app is polling the DB...')

    # Get last movement date
    last_movement_date = dbod.getLastMovementDate()
    if DEBUG:
        print('Last movement date:',last_movement_date)
    
    # Get current delivered and recorded luminosity at last movement
    move_del_lumi, move_rec_lumi = get_del_rec_intlumi(year=last_movement_date.year, dt=last_movement_date)
    if DEBUG:
        print('Delivered lumi at last movement:',move_del_lumi)
        print('Recorded lumi at last movement:',move_rec_lumi)
    
    # Get current delivered and recorded luminosity
    tot_del_lumi, tot_rec_lumi = get_yearly_del_rec_intlumi_now()
    if DEBUG:
        print('Current delivered lumi:',tot_del_lumi)
        print('Current recorded lumi:',tot_rec_lumi)

    # Get last saved lumis
    last_del_lumi = 0
    last_rec_lumi = 0

    last_lumis = dbod.getLastLumis()
    if len(last_lumis) > 0:
        _,last_del_lumi, last_rec_lumi,_,_ = last_lumis

    if DEBUG:
        print('Last saved delivered lumi:',last_del_lumi)
        print('Last saved recorded lumi:',last_rec_lumi)

    # Compute differences
    del_lumi_diff = tot_del_lumi - move_del_lumi
    rec_lumi_diff = tot_rec_lumi - move_rec_lumi
    if DEBUG:
        print('Delivered lumi since last movement:',del_lumi_diff)
        print('Recorded lumi since last movement:',rec_lumi_diff)

    # Save to DB if different from last entry
    if not (isclose(last_del_lumi,tot_del_lumi,rel_tol=1e-4) and  isclose(last_rec_lumi,tot_rec_lumi,rel_tol=1e-4)):
        # Fill the DB only if the lumi increased
        if tot_del_lumi > last_del_lumi or tot_rec_lumi > last_rec_lumi:
            if DEBUG:
                print('Found new lumi value: saving new lumis to DB')
            dbod.writeLumiAndDiff(now,tot_del_lumi,tot_rec_lumi,del_lumi_diff,rec_lumi_diff)
    
    # Check if thresholds are reached
    del_lumi_passed = del_lumi_diff >= DEL_LUMI_THRESHOLD
    rec_lumi_passed = rec_lumi_diff >= REC_LUMI_THRESHOLD
    if DEBUG:
        print('Delivered lumi threshold passed:',del_lumi_passed)
        print('Recorded lumi threshold passed:',rec_lumi_passed)

    if del_lumi_passed or rec_lumi_passed:
        # Check if notification has already been sent
        if DEBUG:
            print('Lumi threshold reached')
        
        # Get last notification
        last_notification = dbod.getLastNotification()
        if len(last_notification) == 0:
            if DEBUG:
                print('Sending first notification')
            # No notification has been sent yet
            dbod.writeNotification(now,move_del_lumi,move_rec_lumi,del_lumi_diff,rec_lumi_diff)
            notify(dbod)
            return
        
        _,prev_move_del_lumi,prev_move_rec_lumi,prev_del_lumi_diff,prev_rec_lumi_diff = last_notification
        if DEBUG:
            print('Last notification:',prev_move_del_lumi,prev_move_rec_lumi,prev_del_lumi_diff,prev_rec_lumi_diff)

        # Check if notification has already been sent
        if isclose(prev_move_del_lumi,move_del_lumi,rel_tol=1e-4) and isclose(prev_move_rec_lumi,move_rec_lumi,rel_tol=1e-4):
            # We are referring to the same previous movement
            if DEBUG:
                print('Notification already sent once')
            if del_lumi_passed and (del_lumi_diff - prev_del_lumi_diff >= DEL_LUMI_THRESHOLD_REPEAT):
                if DEBUG:
                    print('Sending repeat notification for delivered lumi repeat condition')
                dbod.writeNotification(now,move_del_lumi,move_rec_lumi,del_lumi_diff,rec_lumi_diff)
                notify(dbod)
                return
            elif rec_lumi_passed and (rec_lumi_diff - prev_rec_lumi_diff >= REC_LUMI_THRESHOLD_REPEAT):
                if DEBUG:
                    print('Sending repeat notification for recorded lumi repeat condition')
                dbod.writeNotification(now,move_del_lumi,move_rec_lumi,del_lumi_diff,rec_lumi_diff)
                notify(dbod)
                return
            else:
                if DEBUG:
                    print('Repeat condition not satisfied')
                return
        else:
            if DEBUG:
                print('Sending first notification')
            dbod.writeNotification(now,move_del_lumi,move_rec_lumi,del_lumi_diff,rec_lumi_diff)
            notify(dbod)
            return
    else:
        if DEBUG:
            print('Lumi threshold not reached')
        

if __name__ == '__main__':
    dbod = dbod_handler(lumidbname=LUMIDB_NAME, movementdbname=MOVEMENTDB_NAME)

    # Run the scheduler
    scheduler = BlockingScheduler()
    scheduler.add_job(monitor, 'interval', minutes=CHECK_INTERVAL, args=[dbod])
    try:
        print('Starting scheduler')
        scheduler.start()
            
    except KeyboardInterrupt:
        print('Stopping scheduler')
        scheduler.shutdown()
