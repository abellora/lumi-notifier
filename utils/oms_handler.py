from omsapi import OMSAPI
from utils.credentials import getOMSCredentials
from datetime import datetime

def connect_to_oms():
    usr,pwd = getOMSCredentials()
    oms = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
    oms.auth_oidc(usr,pwd)
    return oms

def get_yearly_del_rec_intlumi_now(year:str='2023') -> tuple:
    """
    Get the currently delivered and recorded integrated luminosity (1/pb) from yearly OMS input.
    Only PROTONS runs are considered.
    """
    oms = connect_to_oms()
    q = oms.query('yearlyluminosity')
    q.filter('year',year,'EQ').filter('runtime_type','PROTONS','EQ')
    q.sort('change_date',asc=True)
    json_data = q.data().json()
    tot_del_lumi = json_data['data'][0]['attributes']['delivered_lumi']
    tot_rec_lumi = json_data['data'][0]['attributes']['recorded_lumi']
    return tot_del_lumi,tot_rec_lumi

def get_del_rec_intlumi_now(year:str='2023') -> tuple:
    """
    Get the currently delivered and recorded integrated luminosity (1/pb) from daily OMS input.
    Only PROTONS runs are considered.
    Currently unused, but might eventually be useful
    """
    oms = connect_to_oms()
    q = oms.query('dailyluminosity')
    q.filter('year',year,'EQ').filter('runtime_type','PROTONS','EQ')
    fields = ['delivered_lumi','recorded_lumi']
    q.attrs(fields)
    q.sort('change_date',asc=False)
    q.paginate(page=1,per_page=365)
    json_data = q.data().json()

    tot_del_lumi = 0
    tot_rec_lumi = 0
    for datum in json_data['data']:
        tot_del_lumi += datum['attributes']['delivered_lumi'] if datum['attributes']['delivered_lumi'] is not None else 0
        tot_rec_lumi += datum['attributes']['recorded_lumi'] if datum['attributes']['recorded_lumi'] is not None  else 0
    return tot_del_lumi,tot_rec_lumi
    
def get_del_rec_intlumi(year:str='2023', dt:datetime=datetime(year=1970,month=1,day=1)) -> tuple:
    """
    Get the delivered and recorded integrated luminosity (1/pb) from daily OMS input to a certain datetime.
    Only PROTONS runs are considered.
    Currently unused, but might eventually be useful
    """
    # Sanity checks
    if dt == datetime(year=1970 ,month=1,day=1):
        print('WARNING: No datetime specified, returning total delivered and recorded luminosity')
        return get_del_rec_intlumi_now(year)
    if dt.year != int(year):
        print('WARNING: Year of datetime does not match year specified, returning total delivered and recorded luminosity')
        return get_del_rec_intlumi_now(year)
    if dt > datetime.now():
        print('WARNING: Datetime is in the future, returning total delivered and recorded luminosity')
        return get_del_rec_intlumi_now(year)
    
    oms = connect_to_oms()
    q = oms.query('dailyluminosity')
    q.filter('year',year,'EQ').filter('runtime_type','PROTONS','EQ')
    q.filter('day',str(dt.timetuple().tm_yday),'LE')
    fields = ['day','delivered_lumi','recorded_lumi']
    q.attrs(fields)
    q.paginate(page=1,per_page=365)
    json_data = q.data().json()

    tot_del_lumi = 0
    tot_rec_lumi = 0
    for datum in json_data['data']:
        tot_del_lumi += datum['attributes']['delivered_lumi'] if datum['attributes']['delivered_lumi'] is not None else 0
        tot_rec_lumi += datum['attributes']['recorded_lumi'] if datum['attributes']['recorded_lumi'] is not None  else 0
    return tot_del_lumi,tot_rec_lumi

if __name__ == '__main__':
    tot_del_lumi,tot_rec_lumi = get_del_rec_intlumi_now()
    print(f'Total delivered lumi (daily): {tot_del_lumi} 1/pb')
    print(f'Total recorded lumi (daily): {tot_rec_lumi} 1/pb')
    tot_del_lumi,tot_rec_lumi = get_yearly_del_rec_intlumi_now()
    print(f'Total delivered lumi (yearly): {tot_del_lumi} 1/pb')
    print(f'Total recorded lumi (yearly): {tot_rec_lumi} 1/pb')
    
    mydate = datetime(year=2023,month=5,day=23)
    tot_del_lumi,tot_rec_lumi = get_del_rec_intlumi(year=str(mydate.year),dt=mydate)
    print(f'Total delivered lumi up to {mydate}: {tot_del_lumi} 1/pb')
    print(f'Total recorded lumi up to {mydate}: {tot_rec_lumi} 1/pb')