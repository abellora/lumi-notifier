from sqlalchemy import create_engine, desc, Table, Column, MetaData, DateTime, Integer, Float
from sqlalchemy.sql import select
from datetime import datetime

from utils.credentials import getDBODCredentials

DEBUG = False

class dbod_handler():
    """Handle the interfacing with the mysql DB table.
    """
    def __init__(self,
                 user: str = '',
                 password: str = '',
                 lumidbname: str = 'lumidb_2023_test',
                 movementdbname: str = 'RPixMotorsDB_2023',
                 host: str = 'dbod-ppsrpixmotorsdb.cern.ch',
                 port: str = '5508',
                 lumi_table: str = 'movement_lumi',
                 notification_table: str = 'notifications',
                 movement_table: str = 'MotorMovements') -> None:
        if (user == '') and (password == ''):
            user,password = getDBODCredentials()
        self._movementdb_connection_string = 'mysql+mysqlconnector://'+user+':'+password+'@'+host+':'+port+'/'+movementdbname
        self._lumidb_connection_string = 'mysql+mysqlconnector://'+user+':'+password+'@'+host+':'+port+'/'+lumidbname
        self._lumi_tablename = lumi_table
        self._notification_tablename = notification_table
        self._movement_tablename = movement_table
        
        # Create table objects
        self._metadata = MetaData()
        self.lumi_table = Table(self._lumi_tablename, self._metadata,
            Column('check_date', DateTime(3)),
            Column('del_lumi', Float),
            Column('rec_lumi', Float),
            Column('del_lumi_diff', Float),
            Column('rec_lumi_diff', Float)
        )
        self.notifications_table = Table(self._notification_tablename, self._metadata,
            Column('notification_time', DateTime(3)),
            Column('prev_del_lumi', Float),
            Column('prev_rec_lumi', Float),
            Column('del_lumi_diff', Float),
            Column('rec_lumi_diff', Float)
        )
        self._movement_table = Table(self._movement_tablename, self._metadata,
            Column('datetime_begin', DateTime(3)),
            Column('datetime_end', DateTime(3)),
            Column('arm', Integer),
            Column('station', Integer),
            Column('rp', Integer),
            Column('position_begin', Float),
            Column('position_end', Float),
            Column('nominal_movement', Float)
        )

    def getLastMovementDate(self) -> datetime:
        """Get the date of the last movement in the DB. In case none was performed, return 1st January of the current year.

        Returns:
            datetime
        """
        if DEBUG:
            print('Called getLastMovementDate')
        engine = create_engine(self._movementdb_connection_string)
        
        select_statement = select(self._movement_table.c.datetime_end).order_by(desc(self._movement_table.c.datetime_end)).limit(1)
        with engine.connect() as conn:
            result = conn.execute(select_statement).fetchall()
            if DEBUG:
                print('Query result:',result,sep='\n')
        if result is not None:
            if DEBUG:
                print('Returning',result[0][0])
            return result[0][0]
        else:
            return datetime(year=datetime.utcnow().year,month=1,day=1)
        
    def getLastLumis(self) -> tuple:
        """Get the last entry of the luminosity table from the DB

        Returns:
            tuple: (del_lumi, rec_lumi)
        """
        if DEBUG:
            print('Called getLastLumis')
        engine = create_engine(self._lumidb_connection_string)
        
        select_statement = select(
            self.lumi_table.c.check_date,
            self.lumi_table.c.del_lumi, 
            self.lumi_table.c.rec_lumi,
            self.lumi_table.c.del_lumi_diff,
            self.lumi_table.c.rec_lumi_diff
        )
        select_statement = select_statement.order_by(desc(self.lumi_table.c.check_date)).limit(1)
        with engine.connect() as conn:
            result = conn.execute(select_statement).fetchall()
            if DEBUG:
                print('Query result:',result,sep='\n')
        if result is not None and len(result) > 0:
            if DEBUG:
                print('Returning',result[0])
            return result[0]
        else:
            return []
            
    def writeLumiAndDiff(self,
                         dt:datetime,
                         del_lumi:float,
                         rec_lumi:float,
                         del_lumi_diff:float,
                         rec_lumi_diff:float) -> None:
        """Write the delivered and recorded luminosity to the DB.
        dt must be in UTC and more recent than the last check in the DB.

        Args:
            dt (datetime): Datetime of the measurement
            del_lumi (float): Delivered luminosity (1/pb)
            rec_lumi (float): Recorded luminosity (1/pb)
            del_lumi_diff (float): Difference in delivered luminosity (1/pb)
            rec_lumi_diff (float): Difference in recorded luminosity (1/pb)
        """
        if DEBUG:
            print('Called writeLumiAndDiff')
        engine = create_engine(self._lumidb_connection_string)
        
        check_statement = select(self.lumi_table.c.check_date).order_by(desc(self.lumi_table.c.check_date)).limit(1)
        with engine.connect() as conn:
            result = conn.execute(check_statement).fetchall()
            if DEBUG:
                print('Last check:',result,sep='\n')
        if result is not None and len(result) > 0:
            if dt <= result[0][0]:
                print('WARNING: Datetime is older than last check, not writing to DB')
                return
        insert_statement = self.lumi_table.insert().values(
            check_date=dt,
            del_lumi=del_lumi,
            rec_lumi=rec_lumi,
            del_lumi_diff=del_lumi_diff,
            rec_lumi_diff=rec_lumi_diff
        )
        with engine.connect() as conn:
            conn.execute(insert_statement)
            conn.commit()
        if DEBUG:
            print('Wrote to DB')

    def writeNotification(self,
                          time:datetime, 
                          prev_del_lumi:float, 
                          prev_rec_lumi:float, 
                          del_lumi_diff:float, 
                          rec_lumi_diff:float) -> None:
        """Write a notification to the DB

        Args:
            time (datetime): Datetime of the notification
            prev_del_lumi (float): Delivered luminosity at previous movement (1/pb)
            prev_rec_lumi (float): Recorded luminosity at previous movement (1/pb)
            del_lumi_diff (float): Difference in delivered luminosity (1/pb)
            rec_lumi_diff (float): Difference in recorded luminosity (1/pb)
        """
        if DEBUG:
            print('Called writeNotification')
        engine = create_engine(self._lumidb_connection_string)
        
        insert_statement = self.notifications_table.insert().values(
            notification_time=time,
            prev_del_lumi=prev_del_lumi,
            prev_rec_lumi=prev_rec_lumi,
            del_lumi_diff=del_lumi_diff,
            rec_lumi_diff=rec_lumi_diff
        )
        with engine.connect() as conn:
            conn.execute(insert_statement)
            conn.commit()
        if DEBUG:
            print('Wrote to DB')

    def getLastNotification(self) -> tuple:
        """Get the last notification from the DB

        Returns:
            tuple: (notification_time, prev_del_lumi, prev_rec_lumi, del_lumi_diff, rec_lumi_diff)
        """
        if DEBUG:
            print('Called getLastNotification')
        engine = create_engine(self._lumidb_connection_string)
        
        select_statement = select(
            self.notifications_table.c.notification_time,
            self.notifications_table.c.prev_del_lumi,
            self.notifications_table.c.prev_rec_lumi,
            self.notifications_table.c.del_lumi_diff,
            self.notifications_table.c.rec_lumi_diff
        )
        select_statement = select_statement.order_by(desc(self.notifications_table.c.notification_time)).limit(1)
        with engine.connect() as conn:
            result = conn.execute(select_statement).fetchall()
            if DEBUG:
                print('Query result:',result,sep='\n')
        if result is not None and len(result) > 0:
            if DEBUG:
                print('Returning',result[0])
            return result[0]
        else:
            return []

# Test with python -m utils.dbod_handler from top directory
if __name__ == '__main__':
    dbod = dbod_handler()
    print('Current UTC time:',datetime.utcnow())
    print('Testing getLastMovementDate()')
    print(dbod.getLastMovementDate())
    print('Testing getLastLumis()')
    print(dbod.getLastLumis())
    print('Testing writeLumiAndDiff()')
    dbod.writeLumiAndDiff(datetime.utcnow(),1,2,3,4)
    dbod.writeLumiAndDiff(datetime(year=2023,month=7,day=24),1,2,3,4)
    print('Testing writeNotification()')
    dbod.writeNotification(datetime.utcnow(),1,2,3,4)
    print('Testing getLastNotification()')
    print(dbod.getLastNotification())