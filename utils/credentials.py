import sys
import os

def getOMSCredentials():
    """Get OMS credentials from environment. If not found, use utils/oms_credentials.txt

    Returns:
        str,str: user,password
    """
    usr = os.getenv('OMS_USER')
    pwd = os.getenv('OMS_PWD')
    if (usr != None) and (pwd != None):
        return usr,pwd
    
    # Get from file if env is not set
    try:
        with open('utils/oms_credentials.txt') as f:
            creds = []
            for line in f.readlines():
                if line.startswith('#'):
                    continue
                else:
                    creds = line.split()
            if len(creds) != 2:
                print('ERROR - Credentials not recognized in utils/oms_credentials.txt')
                sys.exit(1)
            else:
                usr,pwd = creds
        return usr,pwd
    except FileNotFoundError:
        print('ERROR - Credential file not found: you should create a utils/oms_credentials.txt file with your user and password')
        sys.exit(1)

def getDBODCredentials():
    """Get alarm DB credentials from environment. If not found, use utils/alarm_db_credentials.txt

    Returns:
        str,str: user,password
    """
    usr = os.getenv('DBOD_USER')
    pwd = os.getenv('DBOD_PWD')
    if (usr != None) and (pwd != None):
        return usr,pwd
    
    # Get from file if env is not set
    try:
        with open('utils/dbod_credentials.txt') as f:
            creds = []
            for line in f.readlines():
                if line.startswith('#'):
                    continue
                else:
                    creds = line.split()
            if len(creds) != 2:
                print('ERROR - Credentials not recognized in utils/dbod_credentials.txt')
                sys.exit(1)
            else:
                usr,pwd = creds
        return usr,pwd
    except FileNotFoundError:
        print('ERROR - Credential file not found: you should create a utils/dbod_credentials.txt file with your user and password')
        sys.exit(1)

def getCERNSSOCredentials():
    """Get CERN SSO credentials from environment. If not found, use utils/cern_sso_credentials.txt
    Returns:
        str,str: user,password
    """
    usr = os.getenv('CLIENT_ID')
    pwd = os.getenv('CLIENT_SECRET')
    if (usr != None) and (pwd != None):
        return usr,pwd
    
    # Get from file if env is not set
    try:
        with open('utils/cern_sso_credentials.txt') as f:
            creds = []
            for line in f.readlines():
                if line.startswith('#'):
                    continue
                else:
                    creds = line.split()
            if len(creds) != 2:
                print('ERROR - Credentials not recognized in utils/cern_sso_credentials.txt')
                sys.exit(1)
            else:
                usr,pwd = creds
        return usr,pwd
    except FileNotFoundError:
        print('ERROR - Credential file not found: you should create a utils/cern_sso_credentials.txt file with your user and password')
        sys.exit(1)

if __name__ == '__main__':
    usr,pwd = getOMSCredentials()
    print('Your OMS user is:',usr)
    print('Your OMS password is:',pwd)
    
    
    usr,pwd = getDBODCredentials()
    print('Your Alarm DB user is:',usr)
    print('Your Alarm DB password is:',pwd)

    usr,pwd = getCERNSSOCredentials()
    print('Your CERN SSO user is:',usr)
    print('Your CERN SSO password is:',pwd)

