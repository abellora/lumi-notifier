#!/bin/bash
set -x

# Move to the code dir
cd /lumi-notifier

# Add the directory to the python path
export PYTHONPATH=$PYTHONPATH:/lumi-notifier

# Run the lumi monitor (don't buffer output)
python -u monitorapp/run.py 2>&1 & # | tee /.local/monitorapp.log &

echo "Started monitor app"

# Setup flask
export FLASK_APP=flaskapp/app.py

if [ -z "${FLASK_PORT}" ]; then
    export FLASK_PORT=8080
fi

python -m flask run  --host='0.0.0.0' --port=${FLASK_PORT}