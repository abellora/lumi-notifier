from flask import Flask, url_for, session
from flask import render_template, redirect
from authlib.integrations.flask_client import OAuth

import os

from utils.oms_handler import *
from utils.dbod_handler import dbod_handler
from utils.credentials import getCERNSSOCredentials

DEBUG = os.environ.get('FLASK_DEBUG') or False

app = Flask(__name__)

# Flask secret key
app.secret_key = os.urandom(24).hex()

# Stuff for CERN SSO
CLIENT_ID, CLIENT_SECRET = getCERNSSOCredentials()
CERN_SSO = 'https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration'

oauth = OAuth(app)
oauth.register(
    name='cern',
    server_metadata_url = CERN_SSO,
    client_id = CLIENT_ID,
    client_secret = CLIENT_SECRET,
    client_kwargs = {
        'scope': 'openid email profile'
    }
)

if os.getenv('DEV') is not None or DEBUG:
    print('Running in dev environment')
    LUMIDB_NAME = 'lumidb_2023_test'
    MOVEMENTDB_NAME = 'RPixMotorsDB_2023'
else:
    print('Running in prod environment')
    LUMIDB_NAME = 'lumidb_2023'
    MOVEMENTDB_NAME = 'RPixMotorsDB_2023'

@app.route('/')
def homepage():
    # Check if user is logged in via SSO, otherwise redirect to login page
    user = session.get('user')
    if user is None:
        return redirect('/login')

    render_dict = {}

    # Get the last movement date from the DB
    dbod = dbod_handler(lumidbname=LUMIDB_NAME, movementdbname=MOVEMENTDB_NAME)
    
    lastMovementDate = dbod.getLastMovementDate()

    def day_with_suffix(day):
        if 11 <= day <= 13:
            suffix = 'th'
        else:
            suffix = {1: 'st', 2: 'nd', 3: 'rd'}.get(day % 10, 'th')
        return str(day) + suffix

    # Get the day of the month and format it with the appropriate suffix
    formatted_day = day_with_suffix(lastMovementDate.day)
    # Format the datetime object with the modified day string
    formatted_date = lastMovementDate.strftime(f"%d %b. %Y").replace(str(lastMovementDate.day), formatted_day)
    render_dict['last_movement_date'] = formatted_date

    _, curr_del_lumi, curr_rec_lumi, del_lumi_diff, rec_lumi_diff = dbod.getLastLumis()
    render_dict['del_lumi'] = f'{curr_del_lumi/1000:.1f}'
    render_dict['rec_lumi'] = f'{curr_rec_lumi/1000:.1f}'
    render_dict['del_lumi_diff'] = f'{del_lumi_diff/1000:.1f}'
    render_dict['rec_lumi_diff'] = f'{rec_lumi_diff/1000:.1f}'
    render_dict['move_del_lumi'] = f'{(curr_del_lumi - del_lumi_diff)/1000:.1f}'
    render_dict['move_rec_lumi'] = f'{(curr_rec_lumi - rec_lumi_diff)/1000:.1f}'
    
    return render_template('home.html', **render_dict)

@app.route('/login')
def login():
    redirect_uri = url_for('auth', _external=True)
    return oauth.cern.authorize_redirect(redirect_uri)

@app.route('/auth')
def auth():
    token = oauth.cern.authorize_access_token()
    session['user'] = token['userinfo']
    return redirect('/')

@app.route('/favicon.ico')
def favicon():
    return redirect(url_for('static', filename='img/favicon.ico'))
