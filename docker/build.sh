#!/bin/bash

# exit when any command fails; be verbose
set -ex

git config --global user.name '"${SERVICE_USERNAME}"'
git config --global user.email '"${SERVICE_USERNAME}"@cern.ch'
git config --global user.github '"${SERVICE_USERNAME}"'

# omsapi
git clone https://${SERVICE_USERNAME}:${SERVICE_PWD}@gitlab.cern.ch/cmsoms/oms-api-client.git
cd oms-api-client/
pip install -r requirements.txt
python setup.py install
cd ..